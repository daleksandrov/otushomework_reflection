﻿using Newtonsoft.Json;
using System;

namespace OtusHomeWork_Reflection
{
    class TestData
    {
        [JsonProperty]
        int i1, i2, i3, i4, i5;

        public static TestData Get() =>
            new TestData()
            {
                i1 = 1,
                i2 = 2,
                i3 = 3,
                i4 = 4,
                i5 = 5
            };
    }
}
