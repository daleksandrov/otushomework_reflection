﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace OtusHomeWork_Reflection
{
    public static class SerialozatorExt
    {
        public static string ToJson(this object inputObject)
        {
            string json = string.Empty;

            var objectFields = inputObject.GetType().GetFields(BindingFlags.Instance | BindingFlags.NonPublic);

            if (objectFields.Length > 0)
            {
                json = "{" +
                                string.Join(',', objectFields.Select(x => $"\"{x.Name}\":{x.GetValue(inputObject)}")) +
                           "}";
            }
            else throw new InvalidOperationException();

            return json;
        }

        public static T FromJson<T>(this string jsonString) where T : new()
        {
            T result = new T();

            //парсим строку json. Работу регулярки можно здесь посмотреть https://regex101.com/r/d2T5Rw/1
            string pattern = @"[""](?<key>[^""]*)["":]+(?<value>[\d]*)";

            try
            {
                if (string.IsNullOrEmpty(jsonString))
                    throw new Exception("Некорректная входная json строка");

                var match = Regex.Match(jsonString, pattern, RegexOptions.IgnoreCase | RegexOptions.Compiled);

                while (match.Success)
                {
                    if (!match.Groups.ContainsKey("key") || !match.Groups.ContainsKey("value"))
                        throw new Exception("Некорректная входная json строка");

                    var key = match.Groups["key"].Value;

                    //получаем информацию о поле в классе по его имени
                    var fieldInfo = result.GetType().GetField(key, BindingFlags.IgnoreCase |
                                                                    BindingFlags.Instance | 
                                                                    BindingFlags.NonPublic);

                    if (fieldInfo == null)
                        throw new Exception($"Класс {result.GetType().Name} не имеет поля {key}");

                    //используя информацию о типе поля, приводим значение к нужному типу
                    var value = Convert.ChangeType(match.Groups["value"].Value, fieldInfo.FieldType.UnderlyingSystemType);
                    
                    fieldInfo.SetValue(result, value);

                    match = match.NextMatch();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            return result;
        }
    }

}
