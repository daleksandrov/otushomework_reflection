﻿using System;
using System.Diagnostics;
using System.Linq;
using Newtonsoft.Json;

namespace OtusHomeWork_Reflection
{
    class Program
    {
        static void Main(string[] args)
        {
            string jsonString = string.Empty;
            int count = 100000;

            Console.WriteLine("Мой рефлекшен::");
            jsonString = TestREflectionSerializator.SerializeObjectByReflection(count);
            TestREflectionSerializator.DeserializeJsonString(jsonString, count);

            Console.WriteLine("Cтандартный механизм (NewtonsoftJson):");
            jsonString = TestStandartJsonSerializator.SerializeObject(count);
            TestStandartJsonSerializator.DeserializeJsonString(jsonString, count);
        }      

        
    }
}
