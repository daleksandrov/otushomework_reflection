﻿using Newtonsoft.Json;
using OtusHomeWork_Reflection;
using System;
using System.Diagnostics;

public class TestStandartJsonSerializator
{
    public static void DeserializeJsonString(string jsonString, int count)
    {
        TestData testData3 = null;

        Stopwatch stopWatch3 = new Stopwatch();
        stopWatch3.Start();


        for (int i = 0; i < count; i++)
            testData3 = JsonConvert.DeserializeObject<TestData>(jsonString);

        stopWatch3.Stop();

        Console.WriteLine("Время десериализации: " + stopWatch3.ElapsedMilliseconds.ToString());
    }

    public static string SerializeObject(int count)
    {
        TestData testData1 = TestData.Get();
        string jsonString = string.Empty;

        Stopwatch stopWatch2 = new Stopwatch();
        stopWatch2.Start();

        for (int i = 0; i < count; i++)
            jsonString = JsonConvert.SerializeObject(testData1);

        stopWatch2.Stop();

        Console.WriteLine(jsonString);
        Console.WriteLine("Время сериализации: " + stopWatch2.ElapsedMilliseconds.ToString());

        return jsonString;
    }
}
