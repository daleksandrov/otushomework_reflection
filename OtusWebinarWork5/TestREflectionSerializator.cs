﻿using OtusHomeWork_Reflection;
using System;
using System.Diagnostics;

public class TestREflectionSerializator
{
    public static void DeserializeJsonString(string jsonString, int count)
    {
        TestData testData2 = null;
        Stopwatch stopWatch0 = new Stopwatch();
        stopWatch0.Start();

        for (int i = 0; i < count; i++)
            testData2 = jsonString.FromJson<TestData>();

        stopWatch0.Stop();

        Console.WriteLine("Время десериализации: " + stopWatch0.ElapsedMilliseconds.ToString());
    }

    public static string SerializeObjectByReflection(int count)
    {
        TestData testData = TestData.Get();
        string jsonString = string.Empty;

        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();

        for (int i = 0; i < count; i++)
            jsonString = testData.ToJson();

        stopWatch.Stop();

        Console.WriteLine(jsonString);
        Console.WriteLine("Время сериализации (рефлексия): " + stopWatch.ElapsedMilliseconds.ToString());

        return jsonString;
    }
}
